# ProsekHUB Backend

This is a Firebase Cloud Functions project, for testing and deployment instructions, please refer to the official docs.

https://firebase.google.com/docs/functions/

## Note:

Since this function makes request to an external website, the Firebase project this is deployed to can't stay on the free Spark plan (since that one only includes requests to Google services), you need at least Blaze.

# Configuration

Running this function requires external configuration.

## Setting config value

Configuration values can be set using the following command:

> firebase functions:config:set {key}="{value}"

## Configuration values

| KEY | Description |
| -------- | -------- |
| timetable.cookie | Cookie generated after typing password into school website |
