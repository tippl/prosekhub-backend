const admin = require('firebase-admin');
const functions = require('firebase-functions');
const axios = require("axios");
const cheerio = require("cheerio");
const pdfjs = require("pdfjs-dist");
const _ = require("lodash");
const moment = require("moment");
const xml2js = require("xml2js");

admin.initializeApp();
var db = admin.firestore();

const url = "http://www.sps-prosek.cz/pro-studenty/rozvrhy-a-suplovani/"
const urlNews = "http://www.sps-prosek.cz/feed/";
const timetableCollection = "timetables_protected";
const substitutionCollection = "substitutions";
const newsCollection = "news";
const timeTables = [
    {
        type: "CLASS",
        search: "tříd"
    },
    {
        type: "TEACHER",
        search: "učitel"
    },
    {
        type: "ROOM",
        search: "místnost"
    }
]

// Helper functions

function timetableObject(o, type) {
    var text = o.text();
    return {
        name: text,
        url: o.attr("href"),
        type: type,
        created: moment().unix(),
        timestamp: moment(/[0-9]{1,2}. [0-9]{1,2}. 20[0-9]{2}/.exec(text)[0], "DD. MM. YYYY").unix()
    };
}

function substObject(o) {
    var children = o.children("td");
    var text = children.eq(0).text();
    return {
        name: text,
        url: children.eq(1).children().eq(0).attr("href") || "",
        created: moment().unix(),
        timestamp: moment(text.split(" ")[0], "DD.MM.YYYY").unix()
    };
}

function newsObject(o) {
    return {
        title: o.title[0],
        description: o.description[0],
        url: o.link[0],
        created: moment().unix(),
        timestamp: moment(o.pubDate[0]).unix()
    }
}

function getRegex(type) {
    switch (type) {
        case "CLASS":
            return /[1-4][A-Z]{1,3}/m;
        case "TEACHER":
            return /([ ]?[A-ZÁČĎÉĚÍŇÓŘŠŤÚŮÝŽ][a-záčďéěíňóřšťúůýž]+){2,3}/m;
        case "ROOM":
            return /[A-Z][0-9]{3}/m;
    }
    return null;
}

// Data acquision functions

function scrapeData(url)
{
    return axios.get(url, { headers: { cookie: functions.config().timetable.cookie } }).then(res => {
        var $ = cheerio.load(res.data);
        var out = {};
        out.timetables = [];
        out.subst = [];

        timeTables.forEach(i => {
            const search = $(".rozvrh-hodin a:contains('" + i.search + "')").eq(0)
            if (search.length)
                out.timetables.push(timetableObject(search, i.type))
        });

        $(".suplovani tr").each((i, o) => out.subst.push(substObject($(o))));
        return out;
    });
}

function getText(url, regex) {
    return pdfjs.getDocument(url).then(pdf => {
        var pagesPromises = [];
        
        for (var i = 1; i <= pdf.numPages; i++) {
            pagesPromises.push(pdf.getPage(i));
        }

        return Promise.all(pagesPromises);
    }).then(pages => {
        var textPromises = [];
        pages.forEach(page => textPromises.push(page.getTextContent()));
        return Promise.all(textPromises);
    }).then(texts => {
        var out = [];
        texts.forEach(text => text.items.forEach(item => {
            var match = regex.exec(item.str);
            if (match)
                out.push(match[0]);
        }));
        return _.uniq(out);
    });
}

// Update functions

function updateTimetable(cl) {
    var docRef = db.collection(timetableCollection).doc(cl.timestamp.toString() + " - " + cl.type);
    return docRef.get().then(res => {
        if (!res.exists || res.data().url !== cl.url) {
            return getText(cl.url, getRegex(cl.type));
        }
        return null;
    }).then(pages => {
        if (pages !== null)
        {
            cl.pages = pages;
            docRef.set(cl);
        }
        return;
    });
}

function updateNews(url) {
    return axios.get(url).then(res => {
        xml2js.parseString(res.data, (err, result) => {
            if (err)
                throw(err);

            var items = result.rss.channel[0].item;
            for (i = 0; i < 3; i++)
            {
                var item = newsObject(items[i]);
                db.collection(newsCollection).doc(item.timestamp.toString()).set(item);
            }
        });
        return;
    });
}

// Run stuff

exports.updateTimetables = functions.https.onRequest((request, response) => {
    scrapeData(url).then(data => {
        datao = data;
        var promises = [];
        data.timetables.forEach(i => promises.push(updateTimetable(i)));
    
        return Promise.all(
            promises,
            data.subst.forEach(i => db.collection(substitutionCollection).doc(i.timestamp.toString()).set(i)),
            updateNews(urlNews)
        );
    }).then(() => {
        response.status(204).send();
        return;
    }).catch(console.log.bind(console));
});
